package com.example.color_dynamic_list.model

import com.example.color_dynamic_list.model.remote.ColorApi
import com.example.color_dynamic_list.model.remote.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {
    private val colorApi = object : ColorApi {
        override suspend fun getColor(colors: Int): List<Int> {
            var newColors: MutableList<Int> = mutableListOf()

            repeat(colors){
                newColors.add(randomColor)
            }
            return newColors
        }
    }

    suspend fun getColor(colors: Int): List<Int> = withContext(Dispatchers.IO)
    {
        colorApi.getColor(colors)
    }

}
