package com.example.color_dynamic_list.model.remote

interface ColorApi {
    suspend fun getColor(colors: Int): List<Int>
}