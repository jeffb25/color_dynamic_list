package com.example.color_dynamic_list.view.Colors

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.color_dynamic_list.R
import com.example.color_dynamic_list.databinding.DetailsColorBinding
import com.example.color_dynamic_list.databinding.FragmentColorBinding

class ColorDetails : Fragment() {

    private var _binding: DetailsColorBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= DetailsColorBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}