package com.example.color_dynamic_list.view.Colors

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.color_dynamic_list.databinding.FragmentColorBinding
import com.example.color_dynamic_list.model.remote.ColorAdapter
import com.example.color_dynamic_list.model.remote.randomColor
import com.example.color_dynamic_list.viewModel.ColorViewModel

class ColorFragment : Fragment() {

    var newColors: MutableList<Int> = mutableListOf<Int>()

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnColor.setOnClickListener{
            binding.rvList.layoutManager = GridLayoutManager(this.context, 3)
            binding.rvList.adapter = ColorAdapter().apply {
                val colors = binding.textInput.text.toString().toInt()
                colorViewModel.getNewColor(colors)
                colorViewModel.randomColors.observe(viewLifecycleOwner){
                    addColors(it)
                }
            }
            val action =
                ColorFragmentDirections.actionColorFragmentToColorDetails()
            findNavController().navigate(action)
        }
    }






}