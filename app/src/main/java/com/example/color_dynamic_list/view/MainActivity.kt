package com.example.color_dynamic_list.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.color_dynamic_list.R
import com.example.color_dynamic_list.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

}