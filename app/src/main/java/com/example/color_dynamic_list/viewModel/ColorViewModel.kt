package com.example.color_dynamic_list.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.color_dynamic_list.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {

    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<List<Int>>()
    val randomColors: LiveData<List<Int>> get() = _randomColor

    fun getNewColor(colors: Int) = viewModelScope.launch {
        val colorList = repo.getColor(colors)
        _randomColor.value = colorList

    }
}